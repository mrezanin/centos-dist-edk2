From bf88198555ce964377a56176de8e5e9b45e43e25 Mon Sep 17 00:00:00 2001
From: Laszlo Ersek <lersek@redhat.com>
Date: Sat, 6 Jun 2020 01:16:09 +0200
Subject: OvmfPkg/X86QemuLoadImageLib: handle EFI_ACCESS_DENIED from
 LoadImage()
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit

Notes about the RHEL-8.2/20190904-37eef91017ad [edk2-stable201908] ->
RHEL-8.3/20200603-ca407c7246bf [edk2-stable202005] rebase:

- new patch

- the patch is being upstreamed; it's not a backport because the rebase
  deadline is close

- upstream references:
  - https://bugzilla.tianocore.org/show_bug.cgi?id=2785
  - http://mid.mail-archive.com/20200605235242.32442-1-lersek@redhat.com
  - https://edk2.groups.io/g/devel/message/60825
  - https://www.redhat.com/archives/edk2-devel-archive/2020-June/msg00344.html

[downstream note ends, upstream commit message starts]

When an image fails Secure Boot validation, LoadImage() returns
EFI_SECURITY_VIOLATION if the platform policy is
DEFER_EXECUTE_ON_SECURITY_VIOLATION.

If the platform policy is DENY_EXECUTE_ON_SECURITY_VIOLATION, then
LoadImage() returns EFI_ACCESS_DENIED (and the image does not remain
loaded).

(Before <https://bugzilla.tianocore.org/show_bug.cgi?id=2129>, this
difference would be masked, as DxeImageVerificationLib would incorrectly
return EFI_SECURITY_VIOLATION for DENY_EXECUTE_ON_SECURITY_VIOLATION as
well.)

In X86QemuLoadImageLib, proceed to the legacy Linux/x86 Boot Protocol upon
seeing EFI_ACCESS_DENIED too.

Cc: Ard Biesheuvel <ard.biesheuvel@arm.com>
Cc: Jordan Justen <jordan.l.justen@intel.com>
Cc: Philippe Mathieu-Daudé <philmd@redhat.com>
Ref: https://bugzilla.tianocore.org/show_bug.cgi?id=2785
Signed-off-by: Laszlo Ersek <lersek@redhat.com>
---
 .../X86QemuLoadImageLib/X86QemuLoadImageLib.c      | 14 ++++++++++----
 1 file changed, 10 insertions(+), 4 deletions(-)

diff --git a/OvmfPkg/Library/X86QemuLoadImageLib/X86QemuLoadImageLib.c b/OvmfPkg/Library/X86QemuLoadImageLib/X86QemuLoadImageLib.c
index ef753be7ea..931553c0c1 100644
--- a/OvmfPkg/Library/X86QemuLoadImageLib/X86QemuLoadImageLib.c
+++ b/OvmfPkg/Library/X86QemuLoadImageLib/X86QemuLoadImageLib.c
@@ -320,15 +320,21 @@ QemuLoadKernelImage (
 
   case EFI_SECURITY_VIOLATION:
     //
-    // We are running with UEFI secure boot enabled, and the image failed to
-    // authenticate. For compatibility reasons, we fall back to the legacy
-    // loader in this case. Since the image has been loaded, we need to unload
-    // it before proceeding
+    // Since the image has been loaded, we need to unload it before proceeding
+    // to the EFI_ACCESS_DENIED case below.
     //
     gBS->UnloadImage (KernelImageHandle);
     //
     // Fall through
     //
+  case EFI_ACCESS_DENIED:
+    //
+    // We are running with UEFI secure boot enabled, and the image failed to
+    // authenticate. For compatibility reasons, we fall back to the legacy
+    // loader in this case.
+    //
+    // Fall through
+    //
   case EFI_UNSUPPORTED:
     //
     // The image is not natively supported or cross-type supported. Let's try
-- 
2.18.4

